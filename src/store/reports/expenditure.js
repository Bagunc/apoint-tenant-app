export default {
  namespaced: true,
  state: {
    name: null,
    sum: null,
    description: null,
    invoiceDate: null,
    invoiceTaxes: null,
    invoiceAttachements1: null,
    invoiceAttachements2: null,
    paymentMethods: "כרטיס אשראי",
    paymentDate: "",
    dueDate: null,
    referenceNumber: null,
  },
  mutations: {
    seName: (state, payload) => state.name = payload,
    setSum: (state, payload) => state.sum = payload,
    setDescription: (state, payload) => state.description = payload,
    setInvoiceDate: (state, payload) => state.invoiceDate = payload,
    setInvoiceTaxes: (state, payload) => state.invoiceTaxes = payload,
    setinvoiceAttachements1: (state, payload) => state.invoiceAttachements1 = payload,
    setInvoiceAttachements2: (state, payload) => state.invoiceAttachements2 = payload,
    setPaymentMethods: (state, payload) => state.paymentMethods = payload,
    setPaymentDate: (state, payload) => state.paymentDate = payload,
    setDueDate: (state, payload) => state.dueDate = payload,
    setReferenceNumber: (state, payload) => state.referenceNumber = payload,
  },
  getters: {
    getName: state => state.name,
    getSum: state => state.sum,
    getDescription: state => state.description,
    getInvoiceDate: state => state.invoiceDate,
    getInvoiceTaxes: state => state.invoiceTaxes,
    getInvoiceAttachements1: state => state.invoiceAttachements1,
    getInvoiceAttachements2: state => state.invoiceAttachements2,
    getPaymentMethods: state => state.paymentMethods,
    getPaymentDate: state => state.paymentDate,
    getDueDate: state => state.dueDate,
    getReferenceNumber: state => state.referenceNumber,
  },
  actions: {
    async doSave() {
      // const response = await request to server

      const response = { success: true }

      return new Promise(resolve => setTimeout(() => resolve(response), 1500))
    }
  }
}