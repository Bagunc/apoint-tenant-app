import { createStore } from 'vuex'

import __preview from '@/preview/'
import { __LS_BUILDING__ } from '@/configs/constants'
import { __ls__, __f_data__ } from '@/includes/helper'

import account from './account'
import auth from './account/auth'

import services from './services'
import call from './services/call'

import offers from './offers/'
import offer from './offers/offer'
import newOffer from './offers/new'

import announcements from './announcements'
import announcement from './announcements/item'

import expenditureReport from './reports/expenditure'

export default createStore({
  state: {
    loading: false,
    requires: false,

    local: {
      code: 'he',
      dir: 'rtl',
    },
    buildings: null,
    building: __ls__(__LS_BUILDING__) || null,
    
    homeServices: [],
    financialCondition: [],
    serviceCalls: {
      label: [],
      items: [],
    },
  },
  mutations: {
    setLoading: (state, payload) => state.loading = payload,
    setRequires: (state, payload) => state.requires = payload,

    setLocal: (state, payload) => state.local = payload,

    setBuilding: (state, payload) => state.building = payload,
    setBuildings: (state, payload) => state.buildings = payload,

    setServiceCalls: (state, payload) => state.serviceCalls = payload,
    setServiceCall: (state, payload) => {
      state.serviceCalls.items = [payload, ...state.serviceCalls.items]
      console.log(state.serviceCalls)
    },

    setHomeServices: (state, payload) => state.homeServices = payload,

    setFinancialCondition: (state, payload) => state.financialCondition = payload,
  },
  getters: {
    getLoading: state => state.loading,
    getRequires: state => state.requires,

    getLocal: state => state.local,

    getServiceCalls: state => state.serviceCalls.items,
    getServiceCallsLabels: state => state.serviceCalls.labels,
    getServiceCallsLabel: state => id => state.serviceCalls.labels[id],
    getServiceCallsBy: state => (query, smartFetch) => __f_data__(state.serviceCalls.items, query, smartFetch),
    getCurrentServiceCalls: (state, getters) => getters.getServiceCallsBy({ buildingId: state.building }, false),
    
    getHomeServices: state => state.homeServices,
    getHomeServicesBy: state => (query, smartFetch) => __f_data__(state.homeServices, query, smartFetch),
    
    getFinancialCondition: state => state.financialCondition,
    getFinancialConditionBy: state => (query, smartFetch) => __f_data__(state.financialCondition, query, smartFetch),

    getBuilding: state => state.building,
    getBuildings: state => state.buildings,
    getBuildingBy: state => query => __f_data__(state.buildings, query),
    getCurrentBuilding: (state, getters) => __f_data__(state.buildings, { id: getters.getBuilding }),

    isAuthUser: (state, getters) => getters['account/authenticated'] && state.building,
  },
  actions: {
    async doLoading({ commit, getters }, payload) {
      commit('setLoading', payload)

      return new Promise(resolve => resolve(getters.getLoading))
    },
    doLanguageSwitch({ commit, dispatch }, payload) {
      commit('setLocal', payload)

      dispatch('doLocalization')
    },
    doLocalization({ getters }) {
      const { code, dir } = getters.getLocal
      
      document.body.setAttribute('class', code)
      document.documentElement.setAttribute('class', code)

      document.body.setAttribute('dir', dir)
      document.documentElement.setAttribute('dir', dir)

      document.documentElement.setAttribute('lang', code)
    },
    async doBuildingChoose({ commit, getters, dispatch }) {
      // await get building data request

      const response = { success: false }

      if (getters.getBuilding) {
        __ls__(__LS_BUILDING__, getters.getBuilding)

        response.success = true

        commit('setRequires', false);

        await dispatch('doRequires').then(response => 
          response.success && 
          dispatch('doLoading', false)
        )
      }
      
      return new Promise(resolve => setTimeout(() =>
        resolve(response), 1000)
      )
    },
    async doRequires({ commit, getters, dispatch }) {
      // await get app required data

      if (getters.getRequires) return

      commit('setLoading', true)

      const response = { success: false }

      dispatch('services/doFetch')
      dispatch('announcements/doFetch')

      commit('setBuildings', __preview('buildings'))
      commit('setHomeServices', __preview('home-service'))
      commit('setServiceCalls', __preview('service-calls'))
      commit('setFinancialCondition', __preview('financial-condition'))

      commit('setRequires', true)
      
      return new Promise(resolve => {
        setTimeout(() => (response.success = true) && resolve(response), 1000)
      })
    },
  },
  modules: {
    auth,
    account,
    
    call,
    services,

    offer,
    offers,
    newOffer,

    announcement,
    announcements,

    expenditureReport,
  }
})
