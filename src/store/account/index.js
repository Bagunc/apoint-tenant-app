import { __ls__, __ls_rm__ } from '@/includes/helper'
import { __LS_TOKEN__, __LS_LOGIN_PHONE__ } from '@/configs/constants'

export default{
  namespaced: true,
  state: {
    data: {
      id: null,
    },
    token: __ls__(__LS_TOKEN__) || null,
  },
  mutations: {
    setToken: (state, payload) => state.token = payload,
    setUserData: (state, payload) => state.data = {...state.data, ...payload},
  },
  getters: {
    getId: state => state.data.id,
    getToken: state => state.token,

    authenticated: state => state.token,
  },
  actions: {
    async signIn({ commit }, payload) {
      // await response get user data by token (payload)
      commit('setToken', payload)

      __ls__(__LS_TOKEN__, payload)
      __ls_rm__(__LS_LOGIN_PHONE__)

      const response = { success: true, id: 1, username: "admin" }
      
      return new Promise(resolve => {
        setTimeout(() => {
          commit('setUserData', response)

          resolve(response)
        }, 1000)
      })
    },
    signUp() {},
    signOut({ commit }) {
      commit('setToken', null)
      __ls_rm__(__LS_TOKEN__)
    },
  },
  modules: {
  }
}
