import { __ls__, __ls_rm__ } from '@/includes/helper'
import { __LS_LOGIN_PHONE__, __LS_BUILDING__ } from '@/configs/constants'

export default {
  namespaced: true,
  state: {
    code: null,
    phoneNumber: __ls__(__LS_LOGIN_PHONE__),
  },
  mutations: {
    setCode: (state, payload) => state.code = payload,
    setPhoneNumber: (state, payload) => state.phoneNumber = payload,
  },
  getters: {
    getCode: state => state.code,
    getPhoneNumber: state => state.phoneNumber,
  },
  actions: {
    doSignIn: async ({ getters, dispatch, rootGetters }) => {
      // response = await send code with service request

      const response = { success: false }
      if (getters.getPhoneNumber) {
        if (rootGetters['account/authenticated'])
          await dispatch('account/signOut', null, { root: true })

        __ls__(__LS_LOGIN_PHONE__, getters.getPhoneNumber)
       
        response.success = true
      }

      return new Promise(resolve =>
        setTimeout(() => 
          resolve(response), 1000)
      )
    },
    doConfirm: async ({ getters }) => {
      // response = await confirmation request

      const response = { success: false }
      if (getters.getCode) {
        response.token = Math.random().toString(36).substr(2)
      
        response.success = true
      }

      return new Promise(resolve => 
        setTimeout(() =>
          resolve(response), 1000)
      )
    },
    doSignOut: async ({ commit, dispatch }) => {
      // response = await logout request

      commit('setCode', null)
      commit('setPhoneNumber', null)

      dispatch('account/signOut', null, { root: true })

      __ls_rm__(__LS_BUILDING__)
      __ls_rm__(__LS_LOGIN_PHONE__)
    }
  }
}