import __preview from '@/preview/'
import { __f_data__ } from '@/includes/helper'

export default {
  namespaced: true,
  state: {
    calls: [],
    services: [],
  },
  mutations: {
    setCalls: (state, payload) => state.calls = payload,
    setCall: (state, payload) => state.calls = [...state.calls, payload],

    setServices: (state, payload) => state.services = payload,
  },
  getters: {
    getCalls: state => state.calls,
    getCall: state => query => __f_data__(state.calls, query),

    getServices: state => state.services,
    getService: state => icon => __f_data__(state.services, { icon }),
  },
  actions: {
    doFetch({ commit }) {
      commit('setServices', __preview('services'))
    }
  }
}