import { __f_data__ } from '@/includes/helper'

export default {
  namespaced: true,
  state: {
    fullName: null,
    attachments: [],
    description: null,
    phoneNumber: null,
  },
  mutations: {
    setFullName: (state, payload) => state.fullName = payload,
    setDescription: (state, payload) => state.description = payload,
    setPhoneNumber: (state, payload) => state.phoneNumber = payload,

    setAttachments: (state, payload) => state.attachments = payload,
    setAttachment: (state, payload) => state.attachments = [...state.attachments, payload],
  },
  getters: {
    getFullName: state => state.fullName,
    getDescription: state => state.description,
    getPhoneNumber: state => state.phoneNumber,

    getAttachments: state => state.attachments,
    getAttachment: state => query => __f_data__(state.attachments, query),
  },
  actions: {
    doSave({ commit, getters }) {
      const date = new Date()
      let d = date.getDate()
      let m = date.getMonth() + 1
          m = m.toString().length < 2 ? `0${m}` : m
      let y = date.getYear()

      const response = {
        success: true,
        item: {
          status: 1,
          date: `${d}/${m}/${y}`,
          fullName: getters.getFullName,
          phoneNumber: getters.getPhoneNumber,
          description: getters.getDescription,
          attachments: getters.getAttachments,
        }
      }

      commit('setServiceCall', response.item, { root: true })

      return new Promise(resolve => {
        setTimeout(() => resolve(response), 1500)
      })
    },
    doTrashAttachment({ commit, getters }, index) {
      const attachments = getters.getAttachments;
            attachments.splice(index, 1)

      commit('setAttachments', attachments)
    }
  }
}