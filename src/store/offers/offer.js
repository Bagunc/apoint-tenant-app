export default {
  namespaced: true,
  state: {
    data: {
      id: null,
      date: null,
      domain: null,
      amount: null,
      dateBidding: null,
      name: null,
      phone: null,
      mail: null,
      attachemnts: null,
      description: null,
    },
    query: null,
  },
  mutations: {
    setQuery: (state, payload) => state.query = payload,
    setData: (state, payload) => state.data = {...state.data, ...payload},
  },
  getters: {
    getData: state => state.data,
    getQuery: state => state.query,
  },
  actions: {
    async doFetch({ commit, dispatch, rootGetters }, query) {
      commit('setQuery', query)

      if (!rootGetters['offers/getItems'].length)
        await dispatch('offers/doFetch', null, { root: true })

      const response = rootGetters['offers/getItem'](query)
      
      commit('setData', response)

      return new Promise(resolve => setTimeout(() => resolve(response), 1000))
    },
  },
}