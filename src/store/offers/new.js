export default {
  namespaced: true,
  state: {
    name: null,
    mail: null,
    phone: null,
    domain: null,
    attachments: [],
    description: null,
  },
  mutations: {
    setName: (state, payload) => state.name = payload,
    setMail: (state, payload) => state.mail = payload,
    setPhone: (state, payload) => state.phone = payload,
    setDomain: (state, payload) => state.domain = payload,
    setAttachments: (state, payload) => state.attachments = payload,
    setDescription: (state, payload) => state.description = payload,
  },
  getters: {
    getName: state => state.name,
    getMail: state => state.mail,
    getPhone: state => state.phone,
    getDomain: state => state.domain,
    getAttachments: state => state.attachments,
    getDescription: state => state.description,
  },
  actions: {
    async doSave({ commit, getters }) {

      // const response = await Request to server
      
      const now = new Date()
      const response = { success: true }

      const data = {
        name: getters.getName,
        mail: getters.getMail,
        phone: getters.getPhone,
        domain: getters.getDomain,
        attachments: getters.getAttachments,
        description: getters.getDescription,
        id: Math.random().toString(36).substr(2),
        date: `${now.getDate()}/${(() => {
          let m = now.getMonth() + 1
          if (m < 10)
            m = `0${m}`

          return m
        })()}/${now.getFullYear()}`,
      }
      
      commit('offers/setItem', data, { root: true })

      return new Promise(resolve => setTimeout(() =>
        resolve(response),
        1500)
      )
    }
  }
}