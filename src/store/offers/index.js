import __preview from '@/preview/'
import { __f_data__ } from '@/includes/helper'

export default {
  namespaced: true,
  state: {
    items: [],
  },
  mutations: {
    setItems: (state, payload) => state.items = payload,
    setItem: (state, payload) => state.items = [payload, ...state.items],
  },
  getters: {
    getItems: state => state.items,
    getItem: state => query => __f_data__(state.items, query),
  },
  actions: {
    async doFetch({ commit }) {
      
      const data = await __preview('offers')
      
      commit('setItems', data)

      return new Promise(resolve => setTimeout(() => resolve(data), 1000))
    }
  }
}