const initialState = {
  id: null,
  date: null,
  text: null,
  buildingId: null,
}

export default {
  namespaced: true,
  state: {
    ...initialState
  },
  mutations: {
    setData: (state, payload) => {
      state.id = payload.id
      
      if ('date' in payload) state.date = payload.date
      if ('text' in payload) state.text = payload.text
      if ('buildingId' in payload) state.buildingId = payload.buildingId
    },
    setDate: (state, payload) => state.date = payload,
    setText: (state, payload) => state.text = payload,
    setBuildingId: (state, payload) => state.buildingId = payload,
  },
  getters: {
    getId: state => state.id,
    getDate: state => state.date,
    getText: state => state.text,
  },
  actions: {
    async doSave({ commit, getters, rootGetters }, mode) {
      // const response = fetch(//to-save)

      const response = { success: true }
      
      const date = (() => {
        if (!getters.getDate) return null

        let val = getters.getDate
        if (typeof getters.getDate === 'string')
          val = new Date(getters.getDate)

        return new Intl.DateTimeFormat('en', { 
          day: '2-digit',
          month: '2-digit',
          year: 'numeric'
        }).format(val)
      })()

      const id = mode === 'UPDATE' ? getters.getId : Math.random().toString(36).substr(2)

      return new Promise(resolve => {
        setTimeout(() => {
          commit(`announcements/${mode === 'UPDATE' ? 'updateItem' : 'setItem'}`, {
            id,
            date,
            text: getters.getText,
            buildingId: rootGetters.getBuilding,
          }, { root: true })

          resolve(response)
        }, 1500)
      })
    },
    doDelete({ getters, dispatch }) {
      // const response = fetch(//to-server)
      const id = getters.getId
      
      return new Promise(resolve =>
        setTimeout(() => {
          const result = dispatch('announcements/doDelete', id, { root: true })
          resolve(result)
        }, 1500)
      )
    },
    doLoad({ commit, rootGetters }, id) {
      const data = id ? rootGetters['announcements/getItemsBy']({ id }) : initialState

      commit('setData', data)
    },
  },
}