import __preview from '@/preview'
import { __f_data__ } from '@/includes/helper'

export default {
  namespaced: true,
  state: {
    items: [],
  },
  mutations: {
    setItems: (state, payload) => state.items = payload,
    setItem: (state, payload) => state.items = [payload, ...state.items],

    updateItem: (state, payload) => {
      if (!payload.id) return

      const index = state.items.findIndex(datum => datum.id == payload.id)

      if (index < 0) return
      
      state.items.splice(index, 1, payload)
    },

    deleteItem: (state, payload) => {
      const index = state.items.findIndex(datum => datum.id == payload)

      if (index < 0) return

      state.items.splice(index, 1)
    }
  },
  getters: {
    getItems: state => state.items,
    getItem: state => index => state.items[index] || null,
    getItemsBy: state => (query, smartFetch = true) => __f_data__(state.items, query, smartFetch),
  },
  actions: {
    async doFetch({ commit }) {
      // const result = await fetch('//')
      const response = { success: true }

      commit('setItems', __preview('announcements'))

      return new Promise(resolve => resolve(response))
    },
    doDelete({ commit }, id) {
      // const resposne = fetch(//to-server)

      const response = { success: true }
      
      commit('deleteItem', id)

      return response
    }
  },
}