import { __PREFIX__ } from '@/configs/constants'

export const __pfx__ = text => __PREFIX__ + text

export const __ls__ = (key, val) => {
  if (val)
    return localStorage.setItem(key, val)
  else
    return localStorage.getItem(key)
}

export const __ls_rm__ = key => localStorage.removeItem(key)

export const __f_data__ = (data, query, smartFetch = true) => {

  if (!data || typeof data !== 'object')
    return smartFetch ? null : []

  const filtered = data.filter(datum => {
    for (let key in query)
      if (query[key] != datum[key])
        return false

    return true
  })

  if (smartFetch)
    return !filtered.length ? null : filtered.length == 1 ? filtered[0] : filtered;
  else
    return filtered
}

export const __c_icon__ = key => {
  switch (key) {
    case 'mail': return 'mail'
    case 'phone': return 'phone-alt'
    case 'whatsapp': return 'whatsapp'
  }

  return null
}

export const __c_link__ = (key, value) => {
  switch (key) {
    case 'phone': return `tel:${value}`
    case 'mail': return `mailto:${value}`
    case 'whatsapp': return `https://wa.me/${value}`
  }
}

export const __event_y__ = event => {

  return ["touchstart", "touchmove", "touchend"].includes(event.type)
      ? event.targetTouches[0].clientY
        : event.clientY
}

export const __event_x__ = event => {

  return ["touchstart", "touchmove", "touchend"].includes(event.type)
      ? event.targetTouches[0].clientX
        : event.clientX
}

export const arrayBufferToBase64 = buffer => {
  var binary = '';
  var bytes = new Uint8Array(buffer);
  var len = bytes.byteLength;
  for (var i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
  }
  return window.btoa(binary);
}

export const urlBase64ToUint8Array = base64String => {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/-/g, '+')
    .replace(/_/g, '/');
 
  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);
 
  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  
  return outputArray;
}