import { __f_data__ } from '@/includes/helper'

export default function __data(db, query) {

  try {
    const data = require(`./${db}.json`)

    if (query)
      return __f_data__(data, query)
      
    return data
  } catch (e) {
    console.error(e)
  }
  return null; 
}