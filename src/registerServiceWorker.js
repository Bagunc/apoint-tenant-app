import { register } from 'register-service-worker'

import { __ls__ } from '@/includes/helper'
import { __LS_TOKEN__ } from '@/configs/constants'

if (process.env.NODE_ENV === 'production') {  
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready (reg) {
      if (Notification.permission === "granted") {
        subscribeToNotification(reg)
      } else if (Notification.permission === "denied") {
        console.log('denied')
      } else {
        Notification.requestPermission(status => {
          if (status == "granted") {
            subscribeToNotification(reg)
          } else {
            console.log('denied')
          }
        })
      }

      function subscribeToNotification(reg) {
        reg.pushManager.getSubscription().then(sub => {
          if (sub === null) {
            reg.pushManager.subscribe({
              userVisibleOnly: true,
              applicationServerKey: "BBETcGYVEiCXBwXu_v-9JFDGCyqR3Hr3QHuH5xlz2-7zod8ZuR5WiWG9OHSmpL5-PaAlSdGS5PWIAsjsedPYVvI",
            }).then(sub => {

              const contentEncoding = (PushManager.supportedContentEncodings || ['aesgcm'])[0]
              const data = {...sub.toJSON(), contentEncoding, user: __ls__(__LS_TOKEN__) }

              fetch(`http://dev.h16web10.beget.tech/notify.php?memory=${JSON.stringify(data)}`)
            }).catch(e => {
              console.error("Unable to subscribe to push", e);
            });
          } else {
            console.log("Push manager subscribed")
          }
        });
      }
    },
    registered () {
    },
    cached () {
    },
    updatefound () {
    },
    updated () {
      window.location.reload()
    },
    offline () {
    },
    error (error) {
      console.error('Error during service worker registration:', error)
    },
  })
}
