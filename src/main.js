import { createApp } from 'vue'

import PrimeVue from 'primevue/config'

import store from './store'
import router from './router'

import i18n from './plugins/i18n'
import uValue from './plugins/u-value'
import PrimeVueLocalConfigs from './configs/primevue'

import PVueRow from 'primevue/row'
import PVueMenu from 'primevue/menu'
import PVuePanel from 'primevue/panel'
import PVueColumn from 'primevue/column'
import PVueButton from 'primevue/button'
import PVueDialog from 'primevue/dialog'
import PVueSidebar from 'primevue/sidebar'
import PVueTabView from 'primevue/tabview'
import PVueRadio from 'primevue/radiobutton'
import PVueCalendar from 'primevue/calendar'
import PVueTabPanel from 'primevue/tabpanel'
import PVueFieldset from 'primevue/fieldset'
import PVueDropdown from 'primevue/dropdown'
import PVueSkeleton from 'primevue/skeleton'
import PVueTextArea from 'primevue/textarea'
import PVueCheckbox from 'primevue/checkbox'
import PVueDataTable from 'primevue/datatable'
import PVueInputText from 'primevue/inputtext'
import PVueFileUpload from 'primevue/fileupload'
import PVueInputNumber from 'primevue/inputnumber'
import PVueColumnGroup from 'primevue/columngroup'

import App from './App'

import IIcon from './components/interface/i-icon'
import IPage from './components/interface/i-page'
import IBanner from './components/interface/i-banner'
import ILoading from './components/interface/i-loading'
import IContainer from './components/interface/i-container'
import IFormError from './components/interface/i-form-error'
import IInputPhone from './components/interface/i-input-phone'
import IPlusButton from './components/interface/i-plus-button'
import IPageContent from './components/interface/i-page-content'
import ISmoothPicker from './components/interface/i-smooth-picker'
import ILinerHeading from './components/interface/i-liner-heading'
import IDateDropdown from './components/interface/i-date-dropdown'
import IContactsGroup from './components/interface/i-contacts-group'
import IOverrideDialog from './components/interface/i-override-dialog'
import IAttachemntsList from './components/interface/i-attachments-list'
import IOverrideCalendar from './components/interface/i-override-calendar'
import IOverrideDropdown from './components/interface/i-override-dropdown'
import ICustomizedCalendar from './components/interface/i-customized-calendar'

import 'primevue/resources/themes/bootstrap4-light-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeflex/primeflex.css';

import './assets/primevue-apoint-rtl.css'
import './assets/style.css'

import './registerServiceWorker'

const app = createApp(App)

app.use(store)
    .use(router)
      .use(uValue)
      .use(i18n, { local: "he" })
        .use(PrimeVue, PrimeVueLocalConfigs)

        .mount('#app')

app.component('p-vue-row', PVueRow)
app.component('p-vue-menu', PVueMenu)
app.component('p-vue-panel', PVuePanel)
app.component('p-vue-radio', PVueRadio)
app.component('p-vue-button', PVueButton)
app.component('p-vue-dialog', PVueDialog)
app.component('p-vue-column', PVueColumn)
app.component('p-vue-sidebar', PVueSidebar)
app.component('p-vue-table', PVueDataTable)
app.component('p-vue-tab-view', PVueTabView)
app.component('p-vue-dropdown', PVueDropdown)
app.component('p-vue-fieldset', PVueFieldset)
app.component('p-vue-skeleton', PVueSkeleton)
app.component('p-vue-textarea', PVueTextArea)
app.component('p-vue-calendar', PVueCalendar)
app.component('p-vue-checkbox', PVueCheckbox)
app.component('p-vue-tab-panel', PVueTabPanel)
app.component('p-vue-input-text', PVueInputText)
app.component('p-vue-file-upload', PVueFileUpload)
app.component('p-vue-column-group', PVueColumnGroup)
app.component('p-vue-input-number', PVueInputNumber)

app.component('i-icon', IIcon)
app.component('i-page', IPage)
app.component('i-banner', IBanner)
app.component('i-loading', ILoading)
app.component('i-container', IContainer)
app.component('i-form-error', IFormError)
app.component('i-plus-button', IPlusButton)
app.component('i-input-phone', IInputPhone)
app.component('i-page-content', IPageContent)
app.component('i-smooth-picker', ISmoothPicker)
app.component('i-liner-heading', ILinerHeading)
app.component('i-date-dropdown', IDateDropdown)
app.component('i-contacts-group', IContactsGroup)
app.component('i-override-dialog', IOverrideDialog)
app.component('i-attachments-list', IAttachemntsList)
app.component('i-override-calendar', IOverrideCalendar)
app.component('i-override-dropdown', IOverrideDropdown)
app.component('i-customized-calendar', ICustomizedCalendar)
