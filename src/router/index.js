import { createRouter, createWebHistory } from 'vue-router'

import store from '../store'
import routes from './routes'

import config from '../../vue.config'

const router = createRouter({
  routes,
  
  history: createWebHistory(),
})

router.beforeEach((to, from, next) => {
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.label);
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);
  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

  if(nearestWithTitle) {
    document.title = `${config.pwa.manifestOptions.short_name} - ${nearestWithTitle.meta.label}`;
  } else if(previousNearestWithMeta) {
    document.title = `${config.pwa.manifestOptions.short_name} - ${previousNearestWithMeta.meta.label}`;
  }

  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => 
    el.parentNode.removeChild(el));

  if(nearestWithMeta)
    nearestWithMeta.meta.metaTags.map(tagDef => {
      const tag = document.createElement('meta');

      Object.keys(tagDef).forEach(key => {
        tag.setAttribute(key, tagDef[key]);
      });

      tag.setAttribute('data-vue-router-controlled', '');

      return tag;
    })
    .forEach(tag => document.head.appendChild(tag));

  if(to.matched.some(recorde => !recorde.meta.public)) {
    if (!store.getters.isAuthUser) {
      store.dispatch('auth/doSignOut')

      return next({ name: 'Login' })
    }
  } else if (to.matched.some(recorde => recorde.meta.public === 'only')) {
    if (store.getters.isAuthUser)
      return next({ 'name': 'Dashboard' })
  }

  next();
})

export default router
