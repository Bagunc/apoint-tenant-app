import { __ } from '@/plugins/i18n/'

export default [
  {
    path: '/login',
    component: () => import('@/layouts/l-login'),
    meta: {
      public: 'only',
    },
    children: [
      {
        path: '',
        name: 'Login',
        meta: {
          public: 'only',
        },
        component: () => import('@/views/v-login/index'),
      },
      {
        path: '/confirm',
        name: 'Confirm',
        meta: {
          public: 'only',
        },
        component: () => import('@/views/v-login/confirm'),
      },
    ],
  },
  {
    path: '/',
    component: () => import('@/layouts/l-default/'),
    children: [
      {
        path: '',
        name: 'Dashboard',
        meta: {
          label: __("Control Panel"),
        },
        component: () => import('@/views/v-dashboard/'),
      },
      {
        name: 'ServiceCall',
        path: '/service-call',
        meta: {
          label: __("Opening a service call"),
        },
        component: () => import('@/views/v-service-call'),
        children: [
          {
            path: '',
            name: 'AllServices',
            meta: {
              label: __("Opening a service call"),
            },
            component: () => import('@/views/v-service-call/all'),
          },
          {
            path: 'open/:service?',
            name: 'OpenServiceCall',
            meta: {
              label: __("Opening a service call"),
            },
            component: () => import('@/views/v-service-call/open'),
          }
        ],
      },
      {
        name: 'BuildingProfile',
        path: '/building-profile',
        meta: {
          label: __("Building profile")
        },
        component: () => import('@/views/v-building-profile/'),
      },
      {
        name: 'ServiceCalls',
        path: '/service-calls/:tab?',
        meta: {
          label: __("Service calls")
        },
        component: () => import('@/views/v-service-calls/'),
      },
      {
        name: 'BudgetReport',
        path: '/budget-report',
        meta: {
          label: __("Financial condition of the building")
        },
        component: () =>  import('@/views/v-budget-report/'),
      },
      {
        name: 'UserProfile',
        path: '/user-profile',
        meta: {
          label: __("User profile")
        },
        component: () =>  import('@/views/v-user-profile/'),
      },
      {
        name: 'TenantRoom',
        path: '/tenant-room',
        meta: {
          label: __("Tenants' room")
        },
        component: () =>  import('@/views/v-tenant-room/'),
      },
      {
        path: '/financial-report',
        meta: {
          label: __("Details of financial situation")
        },
        component: () => import('@/views/v-financial-report/'),
        children: [
          {
            name: 'FinancialReport',
            path: '',
            meta: {
              label: __("Details of financial situation")
            },
            component: () => import('@/views/v-financial-report/list'),
          },
          {
            name: 'FinancialReportDetail',
            path: ':id',
            meta: {
              label: __("Details of financial situation")
            },
            component: () => import('@/views/v-financial-report/detail'),
          },
          {
            name: 'PaymentTotal',
            path: 'payment-total',
            meta: {
              label: __("Summary for payment")
            },
            component: () => import('@/views/v-financial-report/payment-total'),
          },
        ]
      },
      {
        path: '/offers',
        meta: {
          label: __("Quotations for approval")
        },
        component: () => import('@/views/v-offers/'),
        children: [
          {
            path: '',
            name: 'Offers',
            meta: {
              label: __("Quotations for approval")
            },
            component: () => import('@/views/v-offers/list'),
          },
          {
            name: 'Offer',
            path: ':id',
            meta: {
              label: __("Price offers")
            },
            component: () => import('@/views/v-offers/detail'),
          },
        ]
      },
      {
        path: '/maintenance-and-facilities',
        meta: {
          label: __("Maintenance and facilities")
        },
        component: () => import('@/views/v-maintenance-and-facilities/'),
        children: [
          {
            path: '',
            name: 'MaintenanceAndFacilities',
            meta: {
              label: __("Maintenance and facilities")
            },
            component: () => import('@/views/v-maintenance-and-facilities/list')
          },
          {
            path: ':id',
            name: 'MaintenanceAndFacilitiesDetail',
            meta: {
              label: __("Maintenance and facilities")
            },
            component: () => import('@/views/v-maintenance-and-facilities/detail')
          }
        ]
      },
      {
        name: 'RecordingExpenses',
        path: '/recording-expenses',
        meta: {
          label: __("Recording expenses")
        },
        component: () => import('@/views/v-recording-expenses'),
      }
    ]
  },
  {
    path: "/:pathMatch(.*)*",
    name: "NotFound",
    meta: {
      public: true,
      label: __("Page not found"),
    },
    component: () => import('@/views/404'),
  }
]