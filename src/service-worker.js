/* eslint-disable no-undef */

workbox.core.skipWaiting();
workbox.core.clientsClaim();

self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

var MSG;
self.addEventListener('push', event => {
  const { data } = event
  var message;
  try {
    message = data.json()
  } catch (e) {
    message = data.text()
  }
  
  MSG = message

  event.waitUntil(self.registration.showNotification(message.title, message));
});

self.addEventListener('notificationclick', event => {

  const { action, notification } = event

  if (action === 'close')
    return notification.close()

  clients.openWindow(MSG.click_action)
  notification.close();
})