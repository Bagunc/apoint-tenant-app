export const __PREFIX__ = '__atapp__'

export const __LS_LOGIN_PHONE__ = __PREFIX__  + 'login_phone'

export const __LS_TOKEN__ = __PREFIX__ + 'token'
export const __LS_BUILDING__ = __PREFIX__  + 'building'