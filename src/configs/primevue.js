import { __ } from '../plugins/i18n'

export default {
  ripple: true,
  locale: {
    startsWith: __('Starts with'),
    contains: __('Contains'),
    notContains: __('Not contains'),
    endsWith: __('Ends with'),
    equals: __('Equals'),
    notEquals: __('Not equals'),
    noFilter: __('No Filter'),
    lt: __('Less than'),
    lte: __('Less than or equal to'),
    gt: __('Greater than'),
    gte: __('Greater than or equal to'),
    dateIs: __('Date is'),
    dateIsNot: __('Date is not'),
    dateBefore: __('Date is before'),
    dateAfter: __('Date is after'),
    clear: __('Clear'),
    apply: __('Apply'),
    matchAll: __('Match All'),
    matchAny: __('Match Any'),
    addRule: __('Add Rule'),
    removeRule: __('Remove Rule'),
    accept: __('Yes'),
    reject: __('No'),
    choose: __('Choose'),
    upload: __('Upload'),
    cancel: __('Cancel'),
    dayNames: [__("Sunday"), __("Monday"), __("Tuesday"), __("Wednesday"), __("Thursday"), __("Friday"), __("Saturday")],
    dayNamesShort: [__("Sun"), __("Mon"), __("Tue"), __("Wed"), __("Thu"), __("Fri"), __("Sat")],
    dayNamesMin: [__("Su"),__("Mo"),__("Tu"),__("We"),__("Th"),__("Fr"),__("Sa")],
    monthNames: [__("January"),__("February"),__("March"),__("April"),__("May"),__("June"),__("July"),__("August"),__("September"),__("October"),__("November"),__("December")],
    monthNamesShort: [__("Jan"), __("Feb"), __("Mar"), __("Apr"), __("May"), __("Jun"),__("Jul"), __("Aug"), __("Sep"), __("Oct"), __("Nov"), __("Dec")],
    today: __('Today'),
    weekHeader: __('Wk'),
    firstDayOfWeek: 0,
    dateFormat: __('mm/dd/yy'),
    weak: __('Weak'),
    medium: __('Medium'),
    strong: __('Strong'),
    passwordPrompt: __('Enter a password'),
    emptyFilterMessage: __('No results found'),
    emptyMessage: __('No available options'),
  }
}