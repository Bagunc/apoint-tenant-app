import { __ } from '../i18n/'

export const __u_value__ = ({ unit, value }) => {
  switch (unit) {
    case 'square-meter': return `${value} ${__("m<sup>2</sup>")}`
    case 'P-NIS': return `${value} ₪`
    case 'number-f': return new Intl.NumberFormat().format(value)
  }
}

export default {
  install: (app, options) => {
    app.config.globalProperties.__u_value__  = props => __u_value__(props)

    app.provide('u-value', options)
  }
}