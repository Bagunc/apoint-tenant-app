module.exports = {
  chainWebpack: config => {
    config.plugin('html')
          .tap(args => {
              args[0].title = "Build App"
              return args
          })
  },

  runtimeCompiler: true,
  
  pwa: {
    name: 'APoint Build App',
    themeColor: '#277dbe',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'default',

    manifestOptions: {
      display: 'standalone',
      short_name: 'Build App',
      background_color: '#fff',
      orientation: 'landscape'
    },
   
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js',
    }
  }
}